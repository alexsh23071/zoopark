import { DatePicker, DateTimePicker } from '@material-ui/pickers';
import { Dayjs } from 'dayjs';

import styles from './addDatePickerInfo.module.css';

type AddDatePickerInfoProps = {
    type: 'DateTimePicker' | 'DatePicker';
    disabled?: boolean;
    value?: Dayjs | null;
    onChange: (value: Dayjs | null) => void;
    error?: boolean;
    maxDate?: Dayjs | null;
    minDate?: Dayjs | null;
};

export function AddDatePickerInfo({
    type,
    value,
    onChange,
    disabled,
    error,
    maxDate,
    minDate,
}: AddDatePickerInfoProps) {
    return (
        <div className={styles.Card_item}>
            {type === 'DateTimePicker' && (
                <DateTimePicker
                    error={error}
                    disabled={disabled}
                    cancelLabel="Отменить"
                    format="DD-MM-YYYY HH:mm"
                    fullWidth
                    ampm={false}
                    value={value}
                    onChange={onChange}
                    maxDate={maxDate ?? ''}
                    minDate={minDate ?? ''}
                />
            )}
            {type === 'DatePicker' && (
                <DatePicker
                    error={error}
                    disabled={disabled}
                    cancelLabel="Отменить"
                    format="DD-MM-YYYY"
                    fullWidth
                    value={value}
                    onChange={onChange}
                    maxDate={maxDate ?? ''}
                    minDate={minDate ?? ''}
                />
            )}
        </div>
    );
}

AddDatePickerInfo.defaultProps = {
    disabled: false,
    error: false,
    maxDate: '2100-01-01T00:00:00',
    minDate: '1900-01-01T00:00:00',
};

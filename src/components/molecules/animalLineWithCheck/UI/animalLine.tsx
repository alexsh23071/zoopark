import { Link } from 'react-router-dom';

import arrowImg from '../assets/chevron-right.svg';
import styles from './animalLine.module.css';

type AnimalLineProps = {
    name?: string;
    location?: string;
    id?: string | number;
    imgPath?: string;
};

export function AnimalLine({ name, location, id, imgPath }: AnimalLineProps) {
    return (
        <Link to={`/animals/${id}`} className={styles.link}>
            <div className={styles.line}>
                {imgPath && (
                    <img
                        className={styles.img}
                        alt="animal"
                        src={`data:image/svg+xml;base64,${imgPath}`}
                    />
                )}
                <div className={styles.firstCell}>{name}</div>
                <div className={styles.secondCell}>{location}</div>
                <img
                    className={styles.arrowImg}
                    src={arrowImg}
                    alt="goToAnimal"
                />
            </div>
        </Link>
    );
}

import { DesktopDatePicker } from '@mui/x-date-pickers';
import { Input } from 'components/atoms/input/input';
import { AddDatePickerInfo } from 'components/molecules/addDatePickerInfo/UI/addDatePickerInfo';
import dayjs, { Dayjs } from 'dayjs';

import styles from './DatePicker.module.css';

type DatePickerInfoProps = {
    title?: string;
    date?: Dayjs | null;
    handleChangeDate: (value: Dayjs | null) => void;
    withOutPadding?: boolean;
    disabled?: boolean;
};

export function DatePicker({
    title,
    date,
    handleChangeDate,
    withOutPadding,
    disabled = true,
}: DatePickerInfoProps) {
    return (
        <DesktopDatePicker
            value={date}
            onChange={handleChangeDate}
            renderInput={({ inputRef, inputProps, InputProps }) => (
                <div
                    ref={inputRef}
                    className={`${styles.datePickerWrapper} ${
                        withOutPadding ? styles.withOutPadding : ''
                    }`}
                >
                    <Input
                        disabled={disabled}
                        onChange={() => {}}
                        title={title}
                        value={dayjs(date).format('DD-MM-YYYY')}
                    />
                    {InputProps?.endAdornment}
                </div>
            )}
        />
    );
}

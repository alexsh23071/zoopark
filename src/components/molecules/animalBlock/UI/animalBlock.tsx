import { Block } from 'components/molecules/block/UI/block';
import { Link } from 'react-router-dom';

import styles from './animalBlock.module.css';

type AnimalBlockProps = {
    name: string;
    gender: string;
    status: string;
    img?: string;
    link: string;
};

export function AnimalBlock({
    name,
    gender,
    status,
    img,
    link,
}: AnimalBlockProps) {
    return (
        <div className={styles.wrapper}>
            <Link to={link}>
                <div className={styles.headerWrapper}>
                    <img
                        className={styles.img}
                        alt="animal"
                        src={`data:image/svg+xml;base64,${img}`}
                    />
                    <div className={styles.textBlockWrapper}>
                        <Block title="Имя" subtitle={name} />
                        <Block title="Пол" subtitle={gender} />
                        <Block title="Статус" subtitle={status} />
                    </div>
                </div>
            </Link>
        </div>
    );
}

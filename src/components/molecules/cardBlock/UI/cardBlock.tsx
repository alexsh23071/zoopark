import { Block } from 'components/molecules/block/UI/block';
import { Link } from 'react-router-dom';

import styles from './cardBlock.module.css';

type CardBlockProps = {
    id?: string | number;
    menuTitles?: string[];
    subtitles?: string[];
    link?: string;
    theme?: string;
};

export function CardlBlock({
    id,
    link = '',
    menuTitles = [],
    subtitles = [],
    theme = 'green',
}: CardBlockProps) {
    return (
        <div className={`${styles.wrapper} ${styles[theme]}`}>
            {menuTitles &&
                menuTitles.map((el, index) => (
                    <Link to={link}>
                        <Block title={el} subtitle={subtitles[index]} />
                    </Link>
                ))}
        </div>
    );
}

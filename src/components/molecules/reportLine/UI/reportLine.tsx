import { Dayjs } from 'dayjs';
import { Link } from 'react-router-dom';

import arrowImg from '../assets/chevron-right.svg';
import styles from './reportLine.module.css';

type ReportLineProps = {
    id?: string | number;
    taskType?: string;
    taskId?: string;
    period?: Dayjs | null | string;
};

export function ReportLine({ id, taskType, taskId, period }: ReportLineProps) {
    return (
        <Link to={`/reports/${id}`} className={styles.link}>
            <div className={styles.line}>
                <div
                    className={styles.firstCell}
                >{`Отчет по задаче № ${taskId}`}</div>
                <div className={styles.firstCell}>{`${taskType}`}</div>
                <div
                    className={styles.secondCell}
                >{`Дата и время выполнения  ${period}`}</div>
                <img
                    className={styles.arrowImg}
                    src={arrowImg}
                    alt="goToAnimal"
                />
            </div>
        </Link>
    );
}

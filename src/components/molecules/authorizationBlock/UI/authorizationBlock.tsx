import { Input } from 'components/atoms/input/input';

import { Button } from '../../../atoms/button/button';
import useAuthorizationBlock from '../logic/useAuthorizationBlock';
import styles from './authorizationBlock.module.css';

export default function AuthorizationBlock() {
    const {
        handleChangeLogin,
        handleChangePassword,
        login,
        password,
        handleLoginOn,
    } = useAuthorizationBlock();
    return (
        <div className={styles.wrapper}>
            <div className={`${styles.mainTitle} mainTitle`}>BotExpress</div>
            <form className={styles.wrapperAuth}>
                <div className={`${styles.title} title`}>Авторизация</div>
                <div className={styles.wrapperInput}>
                    <Input
                        title="Логин"
                        value={login}
                        onChange={handleChangeLogin}
                    />
                </div>
                <div className={styles.wrapperInput}>
                    <Input
                        type="password"
                        title="Пароль"
                        value={password}
                        onChange={handleChangePassword}
                    />
                </div>
                <Button onClick={handleLoginOn}>Войти</Button>
            </form>
        </div>
    );
}

import { setNotification } from 'components/molecules/notifications/reducers/notification';
import { setIsAuthorization } from 'components/pages/authorizationPage/reducers/authorization';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router';

import { setUserAuthecationInfo } from '../reducers/authorization';
import { useLoginAuthMutation } from '../requests/requests';

const useAuthorizationBlock = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const [onLogin, params] = useLoginAuthMutation();

    const handleLoginOn = async () => {
        await onLogin({
            login,
            password,
        });
    };

    useEffect(() => {
        if (params.isSuccess && params.status === 'fulfilled') {
            const { token, id, userRole } = params.data;
            localStorage.setItem('isAuth', 'true');
            localStorage.setItem('token', token);
            localStorage.setItem('userId', id);
            localStorage.setItem('userRole', userRole);
            dispatch(setIsAuthorization({ isAuth: true }));
            dispatch(setUserAuthecationInfo({ token }));
            if (userRole !== 'VETERINARIAN') {
                navigate('/animals');
            } else {
                navigate('/inspections');
            }
        }
        if (params.isError) {
            dispatch(
                setNotification({
                    type: 'error',
                    text: `Ошибка авторизации, попробуйте еще раз`,
                    isActive: true,
                })
            );
        }
    }, [params]);

    const handleChangeLogin = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLogin(e.target.value);
    };
    const handleChangePassword = (e: React.ChangeEvent<HTMLInputElement>) => {
        setPassword(e.target.value);
    };

    return {
        login,
        handleChangeLogin,
        password,
        handleChangePassword,
        handleLoginOn,
    };
};

export default useAuthorizationBlock;

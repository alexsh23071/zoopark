import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        loginAuth: build.mutation({
            query: (params) => ({
                url: `/api/auth/login`,
                method: 'POST',
                body: {
                    ...params,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useLoginAuthMutation } = extendedApi;

import { Input } from 'components/atoms/input/input';
import { TextArea } from 'components/atoms/textarea/textarea';
import dayjs from 'dayjs';

import useCardReport from '../logic/useCardReport';
import styles from './cardReport.module.css';

export function CardReport() {
    const { params, paramsReport } = useCardReport();

    return (
        <div className={styles.wrapperCard}>
            <div className={`${styles.title} title`}>
                Отчет по задаче № {paramsReport?.data?.taskId ?? ''}
            </div>
            <div className={styles.employeeWrapper}>
                <div className={styles.fieldsWrapper}>
                    <Input
                        disabled
                        title="Тип задачи"
                        value={paramsReport?.data?.taskType ?? ''}
                    />
                    <div className={styles.field}>
                        <Input
                            disabled
                            title="Количество повторений"
                            value={paramsReport?.data?.repeatType ?? ''}
                        />
                    </div>
                </div>
                <div className={styles.fieldsWrapper}>
                    <Input
                        title="Планируемая дата завершения"
                        value={
                            dayjs(paramsReport?.data?.expiresDateTime).format(
                                'DD-MM-YYYY HH:mm'
                            ) ?? ''
                        }
                        disabled
                    />
                    <div className={styles.field}>
                        <Input
                            title="Фактическая дата завершения"
                            disabled
                            value={
                                dayjs(
                                    paramsReport?.data?.completedDateTime
                                ).format('DD-MM-YYYY HH:mm') ?? ''
                            }
                        />
                    </div>
                </div>
                <div className={styles.fieldsWrapper}>
                    <Input
                        title="Животное"
                        disabled
                        value={paramsReport?.data?.animalName ?? ''}
                    />
                    <div className={styles.field}>
                        <Input
                            title="Вид"
                            disabled
                            value={paramsReport?.data?.animalKind ?? ''}
                        />
                    </div>
                </div>
                <div className={styles.fieldsWrapper}>
                    <TextArea
                        disabled
                        title="Примечание"
                        value={paramsReport?.data?.note ?? ''}
                    />
                    <div className={styles.field}>
                        <Input
                            title="Сотрудник"
                            disabled
                            value={
                                `${params?.data?.lastName ?? ''} ${
                                    params?.data?.name ?? ''
                                } ${params?.data?.patronymic ?? ''}` ?? ''
                            }
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

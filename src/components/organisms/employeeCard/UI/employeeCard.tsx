import { Input } from 'components/atoms/input/input';
import { roles } from 'utils/types';

import useEmployeeCard from '../logic/useEmployeeCard';
import styles from './employeeCard.module.css';

export function EmployeeCard() {
    const { params } = useEmployeeCard();
    return (
        <div className={styles.wrapperCard}>
            <div className={styles.employeeWrapper}>
                <div className={styles.fieldsWrapper}>
                    <Input
                        title="Фамилия"
                        value={params?.data?.lastName ?? ''}
                        disabled
                    />
                    <div className={styles.field}>
                        <Input
                            title="Имя"
                            disabled
                            value={params?.data?.name ?? ''}
                        />
                    </div>
                    <div className={styles.field}>
                        <Input
                            disabled
                            title="Отчество"
                            value={params?.data?.patronymic ?? ''}
                        />
                    </div>
                </div>
                {/* <div className={styles.inputWrapper}> */}
                <div className={styles.fieldsWrapper}>
                    <Input
                        title="Возраст"
                        disabled
                        value={params?.data?.age ?? ''}
                    />
                    <div className={styles.field}>
                        <Input
                            title="Должность"
                            disabled
                            value={
                                `${
                                    roles[
                                        params?.data
                                            ?.userRole as keyof typeof roles
                                    ]
                                }` ?? ''
                            }
                        />
                    </div>
                </div>
                <div className={styles.inputWrapper}>
                    <Input
                        title="Логин"
                        disabled
                        value={params?.data?.login ?? ''}
                    />
                </div>
            </div>
        </div>
    );
}

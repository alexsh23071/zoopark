import { useEffect } from 'react';
import { useLocation } from 'react-router';

import { useLazyGetUserQuery } from '../requests/requests';

const useEmployeeCard = () => {
    const location = useLocation();
    const currentId = location.pathname.split('/')[2];
    const [getUser, params] = useLazyGetUserQuery();

    useEffect(() => {
        getUser(currentId);
    }, [currentId]);

    return {
        params,
    };
};

export default useEmployeeCard;

import { Input } from 'components/atoms/input/input';
import { AddAnimalInspectionModal } from 'components/molecules/addAnimalInspectionModal/UI/addAnimalInspectionModal';
import { Dayjs } from 'dayjs';
import { Link } from 'react-router-dom';
import { IUser } from 'utils/types';

import { DropDown } from '../../../atoms/dropdown/dropdown';
import editImg from '../../../pages/animalCardPage/assets/edit.svg';
import optionalImg from '../../../pages/animalCardPage/assets/options.svg';
import check from '../assets/check.png';
import useAnimalCard from '../logic/useAnimalCard';
import styles from './animalCard.module.css';

type AnimalCardProps = {
    name?: string;
    location?: string;
    gender?: string;
    isEdit?: boolean;
    employee?: string;
    height?: string;
    weight?: string;
    age?: string;
    status?: string;
    featuresOfKeeping?: string;
    externalFeatures?: string;
    isOpen: boolean;
    dateForInspections: Dayjs | null;
    handleClose: () => void;
    handleOpen: () => void;
    handleAddInspections: () => void;
    handleChangeDateForInspections: (value: Dayjs | null) => void;
    handleChangeIsEditable?: () => void;
    handleChangeHeight?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleChangeEmployee: (value: string | number) => void;
    handleChangeGender: (value: string | number) => void;
    handleChangeName?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleChangeLocation?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleChangeWeight?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleChangeStatus: (value: string | number) => void;
    handleChangeAge?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleChangeFeaturesOfKeeping?: (
        e: React.ChangeEvent<HTMLInputElement>
    ) => void;
    handleChangeExternalFeatures?: (
        e: React.ChangeEvent<HTMLInputElement>
    ) => void;
    handleChangeAnimal?: () => void;
    handleSellAnimal?: () => void;
};

export function AnimalCard({
    name,
    location,
    gender,
    isEdit,
    employee,
    height,
    weight,
    age,
    status,
    featuresOfKeeping,
    externalFeatures,
    isOpen,
    dateForInspections,
    handleClose,
    handleOpen,
    handleAddInspections,
    handleChangeDateForInspections,
    handleChangeIsEditable,
    handleChangeHeight,
    handleChangeEmployee,
    handleChangeGender,
    handleChangeName,
    handleChangeLocation,
    handleChangeWeight,
    handleChangeStatus,
    handleChangeAge,
    handleChangeFeaturesOfKeeping,
    handleChangeExternalFeatures,
    handleChangeAnimal,
    handleSellAnimal,
}: AnimalCardProps) {
    const {
        isVisible,
        handleChangeVisibleBlock,
        animal,
        users,
        statuses,
        ref,
        role,
        handleAddTask,
    } = useAnimalCard();
    return (
        <div className={styles.wrapperCard}>
            <AddAnimalInspectionModal
                isOpen={isOpen}
                handleClose={handleClose}
                dateForInspections={dateForInspections}
                handleChangeDateForInspections={handleChangeDateForInspections}
                handleAddInspections={handleAddInspections}
            />
            <div className={styles.buttonsWrapper}>
                {!isEdit && animal?.data?.status !== 'Продан' && (
                    <button
                        className={styles.buttonImg}
                        onClick={handleChangeAnimal}
                    >
                        <img src={check} alt="edit" width="40" />
                    </button>
                )}
                {isEdit && animal?.data?.status !== 'Продан' && (
                    <button
                        onClick={handleChangeIsEditable}
                        className={styles.buttonImg}
                    >
                        <img src={editImg} alt="edit" />
                    </button>
                )}
                {role !== 'VETERINARIAN' && animal?.data?.status !== 'Продан' && (
                    <button
                        className={styles.buttonImg}
                        onClick={handleChangeVisibleBlock}
                    >
                        <img src={optionalImg} alt="options" />
                    </button>
                )}
                {isVisible && (
                    <ul className={styles.wrapperBlock} ref={ref}>
                        {role === 'ADMIN' && (
                            <Link to={`/tasks/add/${animal?.data?.id}`}>
                                <li className={styles.wrapperBlockText}>
                                    <button
                                        className={styles.buttonNotVisible}
                                        onClick={handleAddTask}
                                    >
                                        Создать задачу
                                    </button>
                                </li>
                            </Link>
                        )}
                        <li className={styles.wrapperBlockText}>
                            <button
                                className={styles.buttonNotVisible}
                                onClick={handleOpen}
                            >
                                Отправить к ветеринару
                            </button>
                        </li>
                        {role === 'ADMIN' && (
                            <li className={styles.wrapperBlockText}>
                                <button
                                    className={styles.buttonNotVisible}
                                    onClick={handleSellAnimal}
                                >
                                    Продать животного
                                </button>
                            </li>
                        )}
                    </ul>
                )}
            </div>
            <div className={styles.firstWrapper}>
                <div className={styles.imgWrapper}>
                    <img
                        className={styles.img}
                        src={`data:image/svg+xml;base64,${animal?.data?.animalKindDto?.pic}`}
                        alt="animal"
                    />
                </div>
                <div className={styles.textBlockWrapper}>
                    <div className={styles.field}>
                        <Input
                            title="Имя"
                            disabled={isEdit}
                            value={name ?? ''}
                            onChange={handleChangeName}
                        />
                    </div>
                    <div className={styles.field}>
                        <Input
                            title="Расположение"
                            disabled={isEdit}
                            value={location ?? ''}
                            onChange={handleChangeLocation}
                        />
                    </div>
                    <div className={styles.field}>
                        <DropDown
                            label="Сотрудник"
                            disabled={isEdit}
                            value={employee ?? ''}
                            onChange={handleChangeEmployee}
                            source={users?.content?.map((el: IUser) => ({
                                value: el.id,
                                text: `${el.lastName} ${el.name} ${el.patronymic}`,
                            }))}
                        />
                    </div>
                </div>
            </div>
            <div className={styles.secondTextBlockWrapper}>
                <div className={styles.firstContainer}>
                    <div className={styles.fieldsWrapper}>
                        <div className={styles.inputWrapper}>
                            <DropDown
                                disabled={isEdit}
                                label="Статус (состояние)"
                                value={status ?? ''}
                                onChange={handleChangeStatus}
                                source={statuses?.map((el: string) => ({
                                    value: el,
                                    text: el,
                                }))}
                            />
                        </div>
                        <Input
                            title="Возраст"
                            disabled={isEdit}
                            value={age ?? ''}
                            onChange={handleChangeAge}
                        />
                    </div>
                    <div className={styles.fieldsWrapper}>
                        <div className={styles.inputWrapper}>
                            <Input
                                title="Вес, кг"
                                disabled={isEdit}
                                value={weight ?? ''}
                                onChange={handleChangeWeight}
                            />
                        </div>
                        <Input
                            title="Рост, см"
                            disabled={isEdit}
                            value={height ?? ''}
                            onChange={handleChangeHeight}
                        />
                    </div>
                    <DropDown
                        disabled={isEdit}
                        label="Пол"
                        value={gender ?? ''}
                        onChange={handleChangeGender}
                        source={[
                            { value: 1, text: 'Самка' },
                            { value: 2, text: 'Самец' },
                        ]}
                    />
                    <div className={styles.featuresWrapper}>
                        <Input
                            title="Внешние особенности"
                            disabled={isEdit}
                            value={externalFeatures}
                            onChange={handleChangeExternalFeatures}
                        />
                    </div>
                </div>
                <div className={styles.secondContainer}>
                    <div className={styles.field}>
                        <Input
                            title="Класс"
                            disabled
                            value={
                                animal?.data?.animalKindDto?.animalClass ?? ''
                            }
                        />
                    </div>
                    <div className={styles.field}>
                        <Input
                            title="Отряд"
                            disabled
                            value={animal?.data?.animalKindDto?.squad ?? ''}
                        />
                    </div>
                    <div className={styles.field}>
                        <Input
                            title="Вид"
                            disabled
                            value={animal?.data?.animalKindDto?.kind ?? ''}
                        />
                    </div>
                    <div className={styles.field}>
                        <Input
                            title="Особенности содержания"
                            disabled={isEdit}
                            value={featuresOfKeeping ?? ''}
                            onChange={handleChangeFeaturesOfKeeping}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
}

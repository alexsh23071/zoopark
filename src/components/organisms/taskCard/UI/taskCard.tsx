import { DropDown } from 'components/atoms/dropdown/dropdown';
import { Input } from 'components/atoms/input/input';
import { TextArea } from 'components/atoms/textarea/textarea';
import { AnimalLine } from 'components/molecules/animalLine/UI/animalLine';
import { DateTimePicker } from 'components/molecules/DateTimePicker/UI/DateTimePicker';
import { Dayjs } from 'dayjs';

import editImg from '../../../pages/animalCardPage/assets/edit.svg';
import check from '../assets/check.png';
import styles from './taskCard.module.css';

type Animal = {
    animalId: number | null;
    animalKind: string | null;
    animalName: string | null;
};

type TaskCardProps = {
    repeatCount: string;
    repeats: any;
    completed: string;
    date: Dayjs | null;
    note: string;
    type: string;
    animal: Animal;
    isEdit: boolean;
    handleChangeNote: (e: React.ChangeEvent<HTMLTextAreaElement>) => void;
    handleChangeDate: (value: Dayjs | null) => void;
    handleChangeСompleted: (value: string | number) => void;
    handleChangeIsEditable: () => void;
    hadleChangeTask: () => void;
    handleChangeRepeatCount: (value: string | number) => void;
};

export function TaskCard({
    type,
    repeatCount,
    repeats,
    completed,
    date,
    note,
    animal,
    isEdit,
    hadleChangeTask,
    handleChangeNote,
    handleChangeDate,
    handleChangeСompleted,
    handleChangeIsEditable,
    handleChangeRepeatCount,
}: TaskCardProps) {
    return (
        <div className={styles.wrapperCard}>
            <div className={styles.buttonsWrapper}>
                {isEdit && (
                    <button
                        className={styles.buttonImg}
                        onClick={hadleChangeTask}
                    >
                        <img src={check} alt="edit" width="40" />
                    </button>
                )}
                {!isEdit && (
                    <button
                        onClick={handleChangeIsEditable}
                        className={styles.buttonImg}
                    >
                        <img src={editImg} alt="options" />
                    </button>
                )}
            </div>
            <div className={styles.wrapper}>
                <div className={styles.taskWrapper}>
                    <div className={styles.inputWrapper}>
                        <Input title="Тип задачи" disabled value={type ?? ''} />
                        <div className={styles.field}>
                            <DropDown
                                label="Количество повторений"
                                value={repeatCount}
                                onChange={handleChangeRepeatCount}
                                source={repeats?.map((el: string) => ({
                                    value: el,
                                    text: el,
                                }))}
                                disabled={!isEdit}
                            />
                        </div>
                    </div>
                    <div className={styles.inputWrapper}>
                        <DropDown
                            disabled={!isEdit}
                            label="Статус задачи"
                            value={completed}
                            onChange={handleChangeСompleted}
                            source={[
                                { value: 'true', text: 'Выполнено' },
                                { value: 'false', text: 'Не выполнено' },
                            ]}
                        />
                        <div className={styles.fieldVisible}>
                            <DateTimePicker
                                withOutPadding
                                title="Время выполнения"
                                date={date}
                                handleChangeDate={handleChangeDate}
                            />
                        </div>
                    </div>
                    <div className={styles.textareaWrapper}>
                        <TextArea
                            disabled={!isEdit}
                            title="Примечание"
                            value={note}
                            onChange={handleChangeNote}
                        />
                    </div>
                </div>
                <div className={styles.animalsWrapper}>
                    <div className={`${styles.secondTitle} menuTitle`}>
                        Животное
                    </div>
                    <AnimalLine
                        name={animal?.animalName ?? ''}
                        location={animal?.animalKind ?? ''}
                        id={animal?.animalId ?? ''}
                    />
                </div>
            </div>
        </div>
    );
}

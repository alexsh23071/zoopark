import { Button } from 'components/atoms/button/button';
import { DropDown } from 'components/atoms/dropdown/dropdown';
import { Input } from 'components/atoms/input/input';

import styles from './addEmployeeCard.module.css';

type AddEmployeeCardProps = {
    name?: string;
    handleChangeName?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    lastName?: string;
    handleChangeLastName?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    patronymic?: string;
    handleChangePatronymic: (e: React.ChangeEvent<HTMLInputElement>) => void;
    age?: string;
    handleChangeAge?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    login?: string;
    handleChangeLogin?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    userRole?: string;
    handleChangeUserRole: (editValue: string | number) => void;
    password?: string;
    handleChangePassword?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    handleAddUser?: (e: any) => void;
};

export function AddEmployeeCard({
    name,
    lastName,
    patronymic,
    age,
    userRole,
    login,
    password,
    handleChangeName,
    handleChangeLastName,
    handleChangePatronymic,
    handleChangeAge,
    handleChangeLogin,
    handleChangePassword,
    handleChangeUserRole,
    handleAddUser,
}: AddEmployeeCardProps) {
    return (
        <form onSubmit={handleAddUser} className={styles.wrapperOrders}>
            <div className={styles.fieldsWrapper}>
                <Input
                    title="Фамилия"
                    value={lastName ?? ''}
                    onChange={handleChangeLastName}
                />
                <div className={styles.field}>
                    <Input
                        title="Имя"
                        value={name ?? ''}
                        onChange={handleChangeName}
                    />
                </div>
                <div className={styles.field}>
                    <Input
                        title="Отчество"
                        value={patronymic ?? ''}
                        onChange={handleChangePatronymic}
                    />
                </div>
            </div>
            <div className={styles.fieldsWrapper}>
                <DropDown
                    label="Должность"
                    value={userRole}
                    onChange={handleChangeUserRole}
                    source={[
                        { value: 'ADMIN', text: 'Админ' },
                        { value: 'EMPLOYEE', text: 'Сотрудник' },
                        { value: 'VETERINARIAN', text: 'Ветеринар' },
                    ]}
                />
                <div className={styles.field}>
                    <Input
                        type="number"
                        title="Возраст"
                        value={age}
                        onChange={handleChangeAge}
                    />
                </div>
            </div>
            <div className={styles.fieldsWrapper}>
                <Input
                    title="Логин"
                    value={login}
                    onChange={handleChangeLogin}
                />
                <div className={styles.field}>
                    <Input
                        title="Пароль"
                        value={password}
                        onChange={handleChangePassword}
                    />
                </div>
            </div>
            <div className={styles.buttonWrapper}>
                <Button type="submit">Добавить</Button>
            </div>
        </form>
    );
}

import { setNotification } from 'components/molecules/notifications/reducers/notification';
import dayjs, { Dayjs } from 'dayjs';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';

import {
    useAddAnimalForExhibitionMutation,
    useGetAnimalsForExhibitionsQuery,
    useLazyGetExhibitionsQuery,
} from '../requests/requests';

const useExhibitionsCard = () => {
    const dispatch = useAppDispatch();
    const [isVisible, setIsVisible] = useState(false);
    const [getExhibitions, params] = useLazyGetExhibitionsQuery<any>();
    const [date, setDate] = useState<Dayjs | null>(dayjs());
    const [animal, setAnimal] = useState('');

    const handleChangeAnimal = (value: string | number) => {
        setAnimal(`${value}`);
    };

    const handleChangeDate = (value: Dayjs | null) => {
        setDate(value);
    };
    const handleGetExhibitions = async () => {
        setIsVisible(false);
        await getExhibitions(dayjs(date).format('YYYY-MM-DD'));
    };

    useEffect(() => {
        if (params.isError) {
            dispatch(
                setNotification({
                    type: 'error',
                    text: params.error.data.description,
                    isActive: true,
                })
            );
        }
    }, [params]);

    const { data: animals } = useGetAnimalsForExhibitionsQuery({});
    const [addAnimalForExhibition, paramsAddAnimal] =
        useAddAnimalForExhibitionMutation();

    const handleAddAnimalForExhibition = async () => {
        await addAnimalForExhibition({
            id: animal,
            localDate: dayjs(date).format('YYYY-MM-DD'),
        });
    };

    useEffect(() => {
        if (paramsAddAnimal.isSuccess) {
            dispatch(
                setNotification({
                    type: 'success',
                    text: 'Животное успешно добавлено на выставку',
                    isActive: true,
                })
            );
        }
    }, [paramsAddAnimal]);

    const handleShowAnimalsDropDown = () => {
        setIsVisible(true);
    };

    const handleGoBack = () => {
        setIsVisible(false);
    };
    return {
        isVisible,
        date,
        handleChangeDate,
        handleGetExhibitions,
        params,
        animals,
        animal,
        handleChangeAnimal,
        handleAddAnimalForExhibition,
        handleShowAnimalsDropDown,
        handleGoBack,
    };
};

export default useExhibitionsCard;

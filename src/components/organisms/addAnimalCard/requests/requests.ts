import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getKinds: build.query({
            query: () => ({
                url: 'api/animalKinds/only-kinds',
                method: 'GET',
            }),
        }),
        getStatuses: build.query({
            query: () => ({
                url: 'api/animals/states',
                method: 'GET',
            }),
        }),
        getEmployees: build.query({
            query: () => ({
                url: 'api/users/role/EMPLOYEE?size=9999',
                method: 'GET',
            }),
        }),
    }),
    overrideExisting: false,
});

export const { useGetKindsQuery, useGetStatusesQuery, useGetEmployeesQuery } =
    extendedApi;

import { Button } from 'components/atoms/button/button';
import { DropDown } from 'components/atoms/dropdown/dropdown';
import { Input } from 'components/atoms/input/input';
import { IUser } from 'utils/types';

import {
    useGetEmployeesQuery,
    useGetKindsQuery,
    useGetStatusesQuery,
} from '../requests/requests';
import styles from './addAnimalCard.module.css';

type AddAnimalCardProps = {
    name?: string;
    handleChangeName?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    status?: string;
    handleChangeStatus: (editValue: string | number) => void;
    height?: string;
    handleChangeHeight?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    weight?: string;
    handleChangeWeight?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    age?: string;
    handleChangeAge?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    gender?: string;
    handleChangeGender: (editValue: string | number) => void;
    employee?: string;
    handleChangeEmployee: (editValue: string | number) => void;
    kind: string;
    handleChangeKind: (editValue: string | number) => void;
    location?: string;
    handleChangeLocation?: (e: React.ChangeEvent<HTMLInputElement>) => void;
    animalClass?: string;
    squad?: string;
    handleAddAnimal?: (e: any) => void;
    externalFeatures?: string;
    featuresOfKeeping?: string;
    handleChangeFeaturesOfKeeping?: (
        e: React.ChangeEvent<HTMLInputElement>
    ) => void;
    handleChangeExternalFeatures?: (
        e: React.ChangeEvent<HTMLInputElement>
    ) => void;
};

export function AddAnimalCard({
    name,
    handleChangeName,
    status,
    handleChangeStatus,
    height,
    handleChangeHeight,
    weight,
    handleChangeWeight,
    gender,
    handleChangeGender,
    age,
    handleChangeAge,
    employee,
    handleChangeEmployee,
    location,
    handleChangeLocation,
    kind,
    handleChangeKind,
    animalClass,
    squad,
    handleAddAnimal,
    externalFeatures,
    featuresOfKeeping,
    handleChangeFeaturesOfKeeping,
    handleChangeExternalFeatures,
}: AddAnimalCardProps) {
    const { data: kinds } = useGetKindsQuery({});
    const { data: statuses } = useGetStatusesQuery({});
    const { data: users } = useGetEmployeesQuery({});
    return (
        <form onSubmit={handleAddAnimal} className={styles.wrapperOrders}>
            <div className={styles.fieldsWrapper}>
                <Input
                    title="Имя"
                    value={name ?? ''}
                    onChange={handleChangeName}
                />
                <div className={styles.field}>
                    <DropDown
                        label="Статус (состояние)"
                        value={status}
                        onChange={handleChangeStatus}
                        source={statuses?.map((el: string) => ({
                            value: el,
                            text: el,
                        }))}
                    />
                </div>
                <div className={styles.field}>
                    <DropDown
                        label="Пол"
                        value={gender}
                        onChange={handleChangeGender}
                        source={[
                            { value: 'Самка', text: 'Самка' },
                            { value: 'Самец', text: 'Самец' },
                        ]}
                    />
                </div>
            </div>
            <div className={styles.fieldsWrapper}>
                <Input
                    type="number"
                    title="Рост, см"
                    value={height}
                    onChange={handleChangeHeight}
                />
                <div className={styles.field}>
                    <Input
                        type="number"
                        title="Вес, кг"
                        value={weight}
                        onChange={handleChangeWeight}
                    />
                </div>
                <div className={styles.field}>
                    <Input
                        type="number"
                        title="Возраст"
                        value={age}
                        onChange={handleChangeAge}
                    />
                </div>
            </div>
            <div className={styles.fieldsWrapper}>
                <Input
                    title="Внешние особенности"
                    value={externalFeatures}
                    onChange={handleChangeExternalFeatures}
                />
                <div className={styles.field}>
                    <DropDown
                        label="Вид"
                        value={kind}
                        onChange={handleChangeKind}
                        source={kinds?.map((el: string) => ({
                            value: el,
                            text: el,
                        }))}
                    />
                </div>
            </div>
            <div className={styles.fieldsWrapper}>
                <DropDown
                    label="Сотрудник"
                    value={employee}
                    onChange={handleChangeEmployee}
                    source={users?.content?.map((el: IUser) => ({
                        value: el.id,
                        text: `${el.lastName} ${el.name} ${el.patronymic}`,
                    }))}
                />
                <div className={styles.field}>
                    <Input
                        title="Расположение"
                        value={location}
                        onChange={handleChangeLocation}
                    />
                </div>
            </div>
            <div className={styles.fieldsWrapper}>
                <Input
                    title="Особенности содержания"
                    value={featuresOfKeeping}
                    onChange={handleChangeFeaturesOfKeeping}
                />
            </div>
            <div className={styles.buttonWrapper}>
                <Button type="submit">Добавить</Button>
            </div>
        </form>
    );
}

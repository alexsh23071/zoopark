import { AddAnimalCard } from 'components/organisms/addAnimalCard/UI/addAnimalCard';

import useAnimalPage from '../logic/useAnimalPage';
import styles from './addAnimalPage.module.css';

export default function AddAnimalPage() {
    const {
        name,
        gender,
        height,
        weight,
        location,
        employee,
        status,
        age,
        kind,
        squad,
        animalClass,
        externalFeatures,
        featuresOfKeeping,
        handleChangeHeight,
        handleChangeGender,
        handleChangeName,
        handleChangeWeight,
        handleChangeLocation,
        handleChangeEmployee,
        handleChangeStatus,
        handleChangeAge,
        handleChangeKind,
        handleAddAnimal,
        handleChangeFeaturesOfKeeping,
        handleChangeExternalFeatures,
    } = useAnimalPage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>
                    Добавление животного
                </div>
                <AddAnimalCard
                    employee={employee}
                    name={name}
                    gender={gender}
                    height={height}
                    weight={weight}
                    location={location}
                    status={status}
                    age={age}
                    kind={kind}
                    squad={squad}
                    animalClass={animalClass}
                    externalFeatures={externalFeatures}
                    featuresOfKeeping={featuresOfKeeping}
                    handleChangeStatus={handleChangeStatus}
                    handleChangeHeight={handleChangeHeight}
                    handleChangeGender={handleChangeGender}
                    handleChangeName={handleChangeName}
                    handleChangeEmployee={handleChangeEmployee}
                    handleChangeWeight={handleChangeWeight}
                    handleChangeLocation={handleChangeLocation}
                    handleChangeAge={handleChangeAge}
                    handleChangeKind={handleChangeKind}
                    handleAddAnimal={handleAddAnimal}
                    handleChangeFeaturesOfKeeping={
                        handleChangeFeaturesOfKeeping
                    }
                    handleChangeExternalFeatures={handleChangeExternalFeatures}
                />
            </div>
        </div>
    );
}

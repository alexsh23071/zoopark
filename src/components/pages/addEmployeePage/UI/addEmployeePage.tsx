import { AddEmployeeCard } from 'components/organisms/addEmployeeCard/UI/addEmployeeCard';

import useAddEmployeePage from '../logic/useAddEmployeePage';
import styles from './addEmployeePage.module.css';

export default function AddEmployeePage() {
    const {
        name,
        lastName,
        patronymic,
        age,
        userRole,
        login,
        password,
        handleChangeName,
        handleChangeLastName,
        handleChangePatronymic,
        handleChangeAge,
        handleChangeLogin,
        handleChangePassword,
        handleChangeUserRole,
        handleAddUser,
    } = useAddEmployeePage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>
                    Добавление сотрудника
                </div>
                <AddEmployeeCard
                    name={name}
                    lastName={lastName}
                    patronymic={patronymic}
                    age={age}
                    userRole={userRole}
                    login={login}
                    password={password}
                    handleAddUser={handleAddUser}
                    handleChangeName={handleChangeName}
                    handleChangeLastName={handleChangeLastName}
                    handleChangePatronymic={handleChangePatronymic}
                    handleChangeAge={handleChangeAge}
                    handleChangeLogin={handleChangeLogin}
                    handleChangePassword={handleChangePassword}
                    handleChangeUserRole={handleChangeUserRole}
                />
            </div>
        </div>
    );
}

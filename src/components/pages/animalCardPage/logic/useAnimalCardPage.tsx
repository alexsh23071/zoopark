import { setNotification } from 'components/molecules/notifications/reducers/notification';
import { setVisible } from 'components/organisms/animalCard/reducers/visible';
import dayjs, { Dayjs } from 'dayjs';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';

import {
    useAddInspectionsMutation,
    useChangeAnimalMutation,
    useLazyGetAnimalQuery,
} from '../requests/requests';

const useAnimalCardPage = () => {
    const dispatch = useAppDispatch();
    const [isEdit, setIsEdit] = useState<boolean>(true);
    const locationPath = useLocation();
    const currentId = locationPath.pathname.split('/')[2];
    const handleChangeIsEditable = () => {
        setIsEdit(!isEdit);
    };

    const [getAnimal, animal] = useLazyGetAnimalQuery();
    useEffect(() => {
        if (currentId) {
            getAnimal(currentId);
        }
    }, [currentId, getAnimal]);

    const [name, setName] = useState('');
    const [employee, setEmployee] = useState('');
    const [gender, setGender] = useState('');
    const [height, setHeight] = useState('');
    const [weight, setWeight] = useState('');
    const [age, setAge] = useState('');
    const [status, setStatus] = useState('');
    const [location, setLocation] = useState('');
    const [featuresOfKeeping, setFeaturesOfKeeping] = useState('');
    const [externalFeatures, setExternalFeatures] = useState('');
    const [dateForInspections, setDateForInspections] = useState<Dayjs | null>(
        dayjs()
    );
    const [addInspections, paramsInspection] = useAddInspectionsMutation<any>();

    const handleChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value);
    };
    const handleChangeHeight = (e: React.ChangeEvent<HTMLInputElement>) => {
        setHeight(e.target.value);
    };
    const handleChangeWeight = (e: React.ChangeEvent<HTMLInputElement>) => {
        setWeight(e.target.value);
    };
    const handleChangeLocation = (e: React.ChangeEvent<HTMLInputElement>) => {
        setLocation(e.target.value);
    };
    const handleChangeAge = (e: React.ChangeEvent<HTMLInputElement>) => {
        setAge(e.target.value);
    };
    const handleChangeGender = (value: string | number) => {
        setGender(`${value}`);
    };
    const handleChangeEmployee = (value: string | number) => {
        setEmployee(`${value}`);
    };

    const handleChangeStatus = (value: string | number) => {
        setStatus(`${value}`);
    };
    const handleChangeFeaturesOfKeeping = (
        e: React.ChangeEvent<HTMLInputElement>
    ) => {
        setFeaturesOfKeeping(e.target.value);
    };
    const handleChangeExternalFeatures = (
        e: React.ChangeEvent<HTMLInputElement>
    ) => {
        setExternalFeatures(e.target.value);
    };

    useEffect(() => {
        if (animal.data && animal.data.id) {
            setName(animal.data.name);
            setGender(animal.data.gender);
            setWeight(animal.data.weight);
            setHeight(animal.data.height);
            setLocation(animal.data.location);
            if (!animal.data.userRsDto) {
                setEmployee('Не назначен');
            } else setEmployee(animal.data.userRsDto.id);
            setAge(animal.data.age);
            setStatus(animal.data.status);
            setExternalFeatures(animal.data.externalFeatures);
            setFeaturesOfKeeping(animal.data.featuresOfKeeping);
        }
    }, [animal]);
    const [changeAnimal, params] = useChangeAnimalMutation();
    const [sellAnimal, paramsSelling] = useChangeAnimalMutation();
    const handleChangeAnimal = async () => {
        await changeAnimal({
            id: animal.data.id,
            age,
            externalFeatures,
            featuresOfKeeping,
            gender,
            height: Number(height),
            kind: animal.data.animalKindDto.kind,
            location,
            name,
            status,
            userId: employee === 'Не назначен' ? 0 : employee,
            weight: Number(weight),
        });
    };

    useEffect(() => {
        if (params.isSuccess) {
            setIsEdit(true);
            getAnimal(currentId);
            dispatch(
                setNotification({
                    type: 'success',
                    text: `Успешно обновлено!`,
                    isActive: true,
                })
            );
        }
    }, [params]);

    useEffect(() => {
        if (paramsInspection.isSuccess) {
            dispatch(
                setNotification({
                    type: 'success',
                    text: `Животное добавлено на осмотр!`,
                    isActive: true,
                })
            );
        }
        if (paramsInspection.isError) {
            dispatch(
                setNotification({
                    type: 'error',
                    text: paramsInspection.error.data.description,
                    isActive: true,
                })
            );
        }
    }, [paramsInspection]);

    useEffect(() => {
        if (paramsSelling.isSuccess) {
            getAnimal(currentId);
            dispatch(
                setNotification({
                    type: 'info',
                    text: `Животное продано!`,
                    isActive: true,
                })
            );
        }
    }, [paramsSelling]);

    const [isOpen, setIsOpen] = useState(false);
    const handleClose = () => {
        setIsOpen(false);
    };

    const handleOpen = () => {
        setIsOpen(true);
        dispatch(setVisible({ isVisible: false }));
    };

    const handleChangeDateForInspections = (value: Dayjs | null) => {
        setDateForInspections(value);
    };

    const handleAddInspections = async () => {
        await addInspections({
            id: currentId,
            localDate: dayjs(dateForInspections).format('YYYY-MM-DD'),
        });
        setIsOpen(false);
    };

    const handleSellAnimal = async () => {
        await sellAnimal({
            id: animal.data.id,
            age,
            externalFeatures,
            featuresOfKeeping,
            gender,
            height: Number(height),
            kind: animal.data.animalKindDto.kind,
            location,
            name,
            status: 'Продан',
            userId: employee === 'Не назначен' ? 0 : employee,
            weight: Number(weight),
        });
        dispatch(setVisible({ isVisible: false }));
    };

    return {
        name,
        location,
        gender,
        isEdit,
        animal,
        employee,
        height,
        weight,
        status,
        age,
        featuresOfKeeping,
        externalFeatures,
        isOpen,
        dateForInspections,
        handleClose,
        handleOpen,
        handleChangeIsEditable,
        handleAddInspections,
        handleChangeDateForInspections,
        handleChangeHeight,
        handleChangeEmployee,
        handleChangeGender,
        handleChangeName,
        handleChangeLocation,
        handleChangeWeight,
        handleChangeStatus,
        handleChangeAge,
        handleChangeFeaturesOfKeeping,
        handleChangeExternalFeatures,
        handleChangeAnimal,
        handleSellAnimal,
    };
};

export default useAnimalCardPage;

import { emptySplitApi } from '../../../../api/api';

const extendedApi = emptySplitApi.injectEndpoints({
    endpoints: (build) => ({
        getAnimal: build.query({
            query: (id) => ({
                url: `/api/animals/${id}`,
                method: 'GET',
            }),
        }),
        changeAnimal: build.mutation({
            query: ({ id, ...params }) => ({
                url: `/api/animals/${id}`,
                method: 'PUT',
                body: {
                    ...params,
                },
            }),
        }),
        addInspections: build.mutation({
            query: ({ id, ...params }) => ({
                url: `api/inspection/${id}`,
                method: 'PUT',
                params: {
                    ...params,
                },
            }),
        }),
    }),
    overrideExisting: false,
});

export const {
    useLazyGetAnimalQuery,
    useChangeAnimalMutation,
    useAddInspectionsMutation,
} = extendedApi;

import { TaskCard } from 'components/organisms/taskCard/UI/taskCard';

import useTaskPage from '../logic/useTaskPage';
import styles from './taskCardPage.module.css';

export default function TaskCardPage() {
    const {
        animal,
        repeatCount,
        repeats,
        completed,
        date,
        note,
        type,
        isEdit,
        handleChangeNote,
        handleChangeDate,
        handleChangeСompleted,
        handleChangeIsEditable,
        handleChangeRepeatCount,
        hadleChangeTask,
    } = useTaskPage();
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Карточка задачи</div>
                <TaskCard
                    hadleChangeTask={hadleChangeTask}
                    isEdit={isEdit}
                    animal={animal}
                    type={type}
                    repeatCount={repeatCount}
                    repeats={repeats}
                    completed={completed}
                    date={date}
                    note={note}
                    handleChangeNote={handleChangeNote}
                    handleChangeDate={handleChangeDate}
                    handleChangeСompleted={handleChangeСompleted}
                    handleChangeIsEditable={handleChangeIsEditable}
                    handleChangeRepeatCount={handleChangeRepeatCount}
                />
            </div>
        </div>
    );
}

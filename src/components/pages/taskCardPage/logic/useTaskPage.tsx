import { setNotification } from 'components/molecules/notifications/reducers/notification';
import { useGetRepeatsQuery } from 'components/organisms/addTaskCard/requests/requests';
import dayjs, { Dayjs } from 'dayjs';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router';

import {
    useChangeTaskMutation,
    useLazyGetTaskQuery,
} from '../requests/requests';

type Animal = {
    animalId: number | null;
    animalKind: string | null;
    animalName: string | null;
};

const useTaskPage = () => {
    const dispatch = useAppDispatch();
    const { data: repeats } = useGetRepeatsQuery({});
    const [isEdit, setIsEdit] = useState<boolean>(false);
    const [isVisible, setIsVisible] = useState<boolean>(false);
    const [repeatCount, setRepeatCount] = useState('');
    const [completed, setCompleted] = useState('');
    const [note, setNote] = useState('');
    const [type, setType] = useState('');
    const [animal, setAnimal] = useState<Animal>({
        animalId: null,
        animalKind: null,
        animalName: null,
    });
    const [date, setDate] = useState<Dayjs | null>(dayjs());

    const location = useLocation();
    const currentId = location.pathname.split('/')[2];
    const [getTask, taskParams] = useLazyGetTaskQuery();

    useEffect(() => {
        if (currentId) {
            getTask(currentId);
        }
    }, [currentId]);

    const handleChangeVisibleBlock = () => {
        setIsVisible(!isVisible);
    };

    const handleChangeDate = (value: Dayjs | null) => {
        setDate(value);
    };
    const handleChangeIsEditable = () => {
        setIsEdit(!isEdit);
    };
    const handleChangeRepeatCount = (value: string | number) => {
        setRepeatCount(`${value}`);
    };
    const handleChangeСompleted = (value: string | number) => {
        setCompleted(`${value}`);
    };
    const handleChangeNote = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setNote(e.target.value);
    };
    useEffect(() => {
        if (taskParams.data && taskParams.data.id) {
            setRepeatCount(taskParams.data.repeatType);
            setCompleted(`${taskParams.data.completed}`);
            setNote(taskParams.data.note);
            setDate(taskParams.data.expiresDateTime);
            setType(taskParams.data.type);
            setAnimal({
                animalId: taskParams.data.animalId,
                animalKind: taskParams.data.animalKind,
                animalName: taskParams.data.animalName,
            });
        }
    }, [taskParams]);

    const [changeTask, params] = useChangeTaskMutation();

    const hadleChangeTask = async () => {
        await changeTask({
            id: taskParams.data.id,
            animalId: taskParams.data.animalId,
            completed,
            expiresDateTime: dayjs(date)?.utc().format(),
            note,
            repeatType: repeatCount,
            type,
        });
    };
    useEffect(() => {
        if (params.isSuccess) {
            setIsEdit(false);
            getTask(currentId);
            dispatch(
                setNotification({
                    type: 'success',
                    text: `Задача успешно обновлена!`,
                    isActive: true,
                })
            );
        }
    }, [params]);

    return {
        animal,
        type,
        taskParams,
        repeatCount,
        repeats,
        completed,
        date,
        note,
        isEdit,
        isVisible,
        hadleChangeTask,
        handleChangeVisibleBlock,
        handleChangeNote,
        handleChangeDate,
        handleChangeСompleted,
        handleChangeIsEditable,
        handleChangeRepeatCount,
    };
};

export default useTaskPage;

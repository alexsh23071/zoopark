import { InspectionCard } from 'components/organisms/inspectionCard/UI/inspectionCard';

import styles from './inspectionsPage.module.css';

export default function InspectionsPage() {
    return (
        <div className={styles.main}>
            <div className={styles.wrapper}>
                <div className={`${styles.title} title`}>Осмотры</div>
                <InspectionCard />
            </div>
        </div>
    );
}

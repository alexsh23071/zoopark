import { setNotification } from 'components/molecules/notifications/reducers/notification';
import dayjs, { Dayjs } from 'dayjs';
import { useAppDispatch } from 'hooks/defaulthooks';
import { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router';

import { useAddTaskMutation } from '../requests/requests';

const useTaskPage = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const [type, setType] = useState('');
    const [repeatCount, setRepeatCount] = useState('');
    const [note, setNote] = useState('');
    const [date, setDate] = useState<Dayjs | null>(dayjs());
    const location = useLocation();
    const currentId = location.pathname.split('/')[3];

    const handleChangeDate = (value: Dayjs | null) => {
        setDate(value);
    };
    const handleChangeRepeatCount = (value: string | number) => {
        setRepeatCount(`${value}`);
    };
    const handleChangeType = (e: React.ChangeEvent<HTMLInputElement>) => {
        setType(e.target.value);
    };
    const handleChangeNote = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setNote(e.target.value);
    };
    const [addTask, params] = useAddTaskMutation();
    const handleAddTask = async () => {
        await addTask({
            animalId: currentId,
            completed: false,
            expiresDateTime: date?.utc().format(),
            note,
            repeatType: repeatCount,
            type,
        });
    };

    useEffect(() => {
        if (params.isSuccess) {
            dispatch(
                setNotification({
                    type: 'success',
                    text: `Задача успешно создана!`,
                    isActive: true,
                })
            );
            navigate(`/tasks/${params.data.id}`);
        }
    }, [params]);
    return {
        date,
        handleChangeDate,
        type,
        repeatCount,
        note,
        handleChangeType,
        handleChangeRepeatCount,
        handleChangeNote,
        handleAddTask,
    };
};

export default useTaskPage;

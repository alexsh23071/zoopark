import { useState } from 'react';

import { useGetTasksQuery } from '../requests/requests';

const useTasksPage = () => {
    const [currentPage, setCurrentPage] = useState(0);
    const handleChangePage = (event: any, pageNumber: number) => {
        setCurrentPage(pageNumber - 1);
    };
    const { data: tasks } = useGetTasksQuery<any>(
        {
            page: currentPage,
            size: 9,
        },
        { refetchOnMountOrArgChange: true }
    );
    return { tasks, currentPage, handleChangePage };
};

export default useTasksPage;

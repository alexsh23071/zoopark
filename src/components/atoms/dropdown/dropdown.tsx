import { memo, useEffect, useMemo, useRef, useState } from 'react';

import downImg from './assets/chevron-down.svg';
import upImg from './assets/chevron-up.svg';
import styles from './dropdown.module.css';

type DropDownProps = {
    placeholder?: string;
    disabled?: boolean;
    onChange: (editValue: number | string) => void;
    source?: Array<{ value: number | string; text: string }>;
    value: number | string | undefined;
    label?: string;
};

function CustomDropDown({
    placeholder = '',
    onChange,
    source = [],
    disabled = false,
    value,
    label = '',
}: DropDownProps) {
    const [open, setOpen] = useState(false);
    const ref = useRef<HTMLInputElement>(null);

    const findName = useMemo(() => {
        const foundElement = source?.find(
            (element) => `${element.value}` === `${value}`
        );
        return foundElement?.text;
    }, [value, source]);

    const handleClickOutside = (event: any) => {
        if (ref.current && !ref.current.contains(event.target)) {
            setOpen(false);
        }
    };

    useEffect(() => {
        document.addEventListener('click', handleClickOutside, true);
        return () => {
            document.removeEventListener('click', handleClickOutside, true);
        };
    });

    const handleChangeOpen = () => {
        if (!disabled) setOpen(!open);
    };

    const handleChangeValue = (editValue: number | string) => {
        setOpen(false);
        onChange(editValue);
    };

    const handleChangeValueOnKey = (
        e: React.KeyboardEvent,
        editValue: number | string
    ) => {
        if (e.key === 'Enter') {
            setOpen(false);
            onChange(editValue);
        }
    };

    return (
        <div className={styles.wrapper} ref={ref}>
            <label className={styles.dropdownWrapper}>
                <div className={styles.label}>{label}</div>
                <div
                    role="button"
                    tabIndex={0}
                    className={`${disabled ? styles.disabled : ''} ${
                        open ? styles.openHeader : styles.closeHeader
                    }`}
                    onClick={handleChangeOpen}
                    onKeyPress={handleChangeOpen}
                >
                    <input
                        disabled
                        className={`${styles.input} Body1`}
                        type="text"
                        placeholder={placeholder}
                        value={findName || value || ''}
                    />
                    <img
                        className={styles.img}
                        src={open ? upImg : downImg}
                        alt="arror"
                    />
                </div>
                {open && (
                    <ul className={styles.optionsWrapper}>
                        {source?.map((element) => (
                            <li
                                tabIndex={0}
                                key={`dropdown${element.value}`}
                                className={`${styles.li}`}
                                role="menuitem"
                                onClick={() => handleChangeValue(element.value)}
                                onKeyPress={(e: React.KeyboardEvent) =>
                                    handleChangeValueOnKey(e, element.value)
                                }
                            >
                                {element.text}
                            </li>
                        ))}
                    </ul>
                )}
            </label>
        </div>
    );
}

export const DropDown = memo(CustomDropDown);
